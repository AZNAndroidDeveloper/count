package uz.azn.lesson22homeworklistview.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import uz.azn.lesson22homeworklistview.AllInfo
import uz.azn.lesson22homeworklistview.R

class CustomListAdapter(
        val context: Activity,
        val imageList: MutableList<Int>,
        val textList: MutableList<String>
) : ArrayAdapter<Any>(
        context,
        R.layout.itmes,
        imageList as MutableList<Any>) {
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val row = inflater.inflate(R.layout.itmes, null, false)
        val image = row.findViewById<ImageView>(R.id.image)
        val text = row.findViewById<TextView>(R.id.name_text_view)
        val countImage = imageList[position]
        image.setImageResource(countImage)
        val countTextView = textList[position]
        text.text = countTextView
        return row
    }
    


}