package uz.azn.lesson22homeworklistview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.children
import uz.azn.lesson22homeworklistview.adapter.CustomListAdapter
import uz.azn.lesson22homeworklistview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    var image = Images.imageList
    var name = Images.nameList
    lateinit var allInfoList: MutableList<AllInfo>
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val allInfo = AllInfo(name, image)
        allInfoList = arrayListOf()
        allInfoList.add(allInfo)
        val adapter = CustomListAdapter(this, image, name)
        binding.listView.adapter = adapter
        binding.listView.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val intent = Intent(applicationContext, InformationActivity::class.java)
                intent.putExtra("name", position)
                startActivity(intent)

            }
    }

}