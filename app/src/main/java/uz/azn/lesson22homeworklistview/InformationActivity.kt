package uz.azn.lesson22homeworklistview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import uz.azn.lesson22homeworklistview.databinding.ActivityInformationBinding

class InformationActivity : AppCompatActivity() {
    private var image = Images.imageList
    private var name = Images.nameList
    private var info = Images.informationList
    override fun onCreate(savedInstanceState: Bundle?) {
        val binding by lazy { ActivityInformationBinding.inflate(layoutInflater) }
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val count = intent.getIntExtra("name", 0)
        Log.d("user", count.toString())
        binding.imageTwo.setImageResource(image[count])
        binding.nameTwo.text = name[count]
        binding.infoTextView.text = info[count]

//       val adapter  = CustomListAdapterInfo(this, info, name, image,count)
        // binding.listView.adapter = adapter
    }
}